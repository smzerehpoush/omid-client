import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { getMovies, saveMovie } from "./services/fakeMovieService";
import { getGenres } from "./services/fakeGenreService";
import NavBar from "./components/navbar";
import NotFound from "./components/notFound";
import LoginForm from "./components/loginForm";
import RegisterForm from "./components/registerForm";
import AddCurrencyExchange from "./components/addCurrencyExchange";
import GetUserCurrenies from "./components/getUserCurrencies"

class App extends Component {
  state = {
    movies: [],
    genres: []
  };

  componentDidMount() {
    const genres = [
      { name: "All Genres", _id: "5b21ca3eeb7f6fbccd471ff8", key: "allGenres" },
      ...getGenres()
    ];
    this.setState({ movies: getMovies(), genres });
  }

  handleItemSelect = activeItem => {
    this.setState({ activeItem });
  };
  handleAddMovie = movie => {
    saveMovie(movie);
    const movies = getMovies();
    this.setState({ movies });
  };
  handleDelete = id => {
    const dbMovies = [...this.state.movies];

    const movies = dbMovies.filter(m => m._id !== id);
    this.setState({
      movies
    });
  };
  handleLike = id => {
    const movies = this.state.movies;

    movies.forEach(element => {
      if (element._id === id) {
        element.liked = element.liked === "true" ? "false" : "true";
      }
    });
    this.setState({
      movies
    });
  };
  render() {
    const items = [
      { _id: 2, name: "Add Currency Exchange", path: "/addCurrencyExchange" },
      { _id: 3, name: "Get User Currency Exchanges", path: "/getUserCurrencies" },
      { _id: 4, name: "Login", path: "/login" },
      { _id: 5, name: "Register", path: "/register" }
    ];
    const { activeItem } = this.state;
    return (
      <div>
        <NavBar
          items={items}
          onSelect={this.handleItemSelect}
          activeItem={activeItem}
        />
        <div>
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterForm} />
            <Route
              path="/addCurrencyExchange"
              component={AddCurrencyExchange}
            />
            <Route path="/not-found" component={NotFound} />
            <Route
              path="/getUserCurrencies"
              component={GetUserCurrenies}
            />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/" exact to="/login" />
            <Redirect to="/not-found" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
