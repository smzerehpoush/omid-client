import React, { Component } from "react";
import axios from "axios";
class CountryComboBox extends Component {
  state = {
    countries: [],
    selectedCountry: "",
    validationError: ""
  };

  componentDidMount() {
    axios
      .get("http://localhost:9898/api/baseInfo/countries/names")
      .then(res => {
        const data = res.data;
        if (data.code !== 0) throw new Error();
        return data.content;
      })
      .then(content => {
        let myCountries = content.countries.map(country => {
          return { value: country.id, display: country.name };
        });
        this.setState({ countries: myCountries });
      })
      .catch(err => console.error(err));
  }

  render() {
    return (
      <div>
        <div>
          {this.props.label + "  "}
          <select
            value={this.state.selectedCountry}
            onChange={e => {
              console.log("selected item " + e.target.value);
              this.setState({
                selectedCountry: e.target.value,
                validationError:
                  e.target.value === ""
                    ? "You must select your favourite team"
                    : ""
              });
            }}
          >
            {this.state.countries.map(team => (
              <option key={team.value} value={team.value}>
                {team.display}
              </option>
            ))}
          </select>
        </div>
        <div style={{ color: "red", marginTop: "5px" }}>
          {this.state.validationError}
        </div>
      </div>
    );
  }
}
export default CountryComboBox;
