import React, { useState, useEffect } from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  OutlinedInput,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import "../styles.css";
import getCookie from "../getCookie"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },table: {
    maxWidth: 500,
    align:"center"
  }
}));

function AddCurrencyExchange(props) {
  const classes = useStyles();
  const [values, setValues] = useState({ from: null, to: null });
  const [options, setOptions] = useState([]);
  const [currencies, setCurrencies] = useState([]);

  useEffect(() => {
    let token = getCookie("token")
    if (token.length === 0)
      props.history.push("/login")
    axios
      .get("http://localhost:9898/api/baseInfo/countries/names")
      .then(res => {
        setOptions(
          res.data.content.countryNames.filter(name => name !== "Iran").sort()
        );
      });
    axios
      .put("http://localhost:9898/api/user/currency", { token })
      .then(res => {
        console.log(res.data)
        setCurrencies(
          res.data.content
        );
      });
  }, []);
  const handleSubmit = () => {
    let token = getCookie("token")
    axios
      .post(
        `http://localhost:9898/api/user/currency`, {
          ...values, token
        }
      )
      .then(res => {
        console.log(res.data)
        if (res.data.code === 0) {
          document.location.reload()
          
        }
        else
          alert(res.data.message)
      });
  };
  const handleReverse = () => {
    setValues(prev => {
      let oldFrom = prev.from;
      return { ...prev, from: prev.to, to: oldFrom };
    });
  };
  const handleDelete = (data)=>{
    let token = getCookie("token")
    axios
      .post(
        `http://localhost:9898/api/user/currencyd`, {
          ...data, token
        }
      )
      .then(res => {
        console.log(res.data)
        if (res.data.code === 0) {
          document.location.reload()
          
        }
        else
          alert(res.data.message)
      });
  }

  return (
    <div>
      <div>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>From</TableCell>
              <TableCell align="left">To</TableCell>
              <TableCell></TableCell>

            </TableRow>
          </TableHead>
          <TableBody>
            {currencies.map(item => (
              <TableRow key={`${item.from}-${item.to}`}>
                <TableCell align="right">{item.from}</TableCell>
                <TableCell align="right">{item.to}</TableCell>
                <TableCell align="right"><button onClick={() => handleDelete({from:item.from,to:item.to})} className="btn btn-secondary btn-sm m-2">delete</button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <div className="App">
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="from">From</InputLabel>
          <Select
            value={values.from || ""}
            onChange={e =>
              setValues(prev => {
                return { ...prev, from: e.target.value };
              })
            }
            input={<OutlinedInput name="from" id="from" />}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {options &&
              options.length !== 0 &&
              options.map(option => (
                <MenuItem value={option} key={option}>
                  {option}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
        <Button variant="contained" onClick={handleReverse}>
          reverse
      </Button>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="to">To</InputLabel>
          <Select
            value={values.to || ""}
            onChange={e =>
              setValues(prev => {
                return { ...prev, to: e.target.value };
              })
            }
            input={<OutlinedInput name="to" id="to" />}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {options &&
              options.length !== 0 &&
              options.map(option => (
                <MenuItem value={option} key={option}>
                  {option}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
        <Button variant="contained" onClick={handleSubmit}>
          +
      </Button>
      </div>
    </div>
  );
}
export default AddCurrencyExchange;
