import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import axios from "axios";

class RegisterForm extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .min(3)
      .label("Username"),
    password: Joi.string()
      .required()
      .min(5)
      .label("Password")
  };

  doSubmit = () => {
    axios
      .post("http://localhost:9898//api/user/signupByUsername", {
        username: this.state.data.username,
        password: this.state.data.password
      })
      .then(res => {
        console.log(res.data);
        if (res.data.code === 0) this.props.history.push("/login");
        else alert(res.data.message);
      });
    console.log("Submitted");
  };

  render() {
    return (
      <div className="m-4">
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Register")}
        </form>
      </div>
    );
  }
}

export default RegisterForm;
