import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from "axios";
import "../styles.css";
import getCookie from "../getCookie"

const useStyles = makeStyles(theme => ({
  root: {
    width: '50%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    maxWidth: 500,
  },
}));
function handleClick(props) {
  props.history.push("/addCurrencyExchange");
}
function GetUserCurrenies(props) {
  const classes = useStyles();
  const [options, setOptions] = useState([]);
  useEffect(() => {
    let token =getCookie("token") 
    if (token.length === 0)
      props.history.push("/login")
    
    axios
      .put("http://localhost:9898/api/user/currency", { token })
      .then(res => {
        console.log(res.data)
        setOptions(
          res.data.content
        );
      });
  }, []);

  return (
    <div className="App">
      <button
        className="btn btn-primary"
        style={{ marginBottom: 20 }}
        onClick={() => handleClick(props)}>
        Add New Exchange
            </button>
      {/* <Link
              className="btn btn-primary"
              style={{ marginBottom: 20 }}
              to={{pathname:"/addCurrencyExchange",
              
               }}>
              Add New Exchange
            </Link> */}
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>From</TableCell>
            <TableCell align="left">To</TableCell>
            <TableCell align="left">Rate</TableCell>

          </TableRow>
        </TableHead>
        <TableBody>
          {options.map(item => (
            <TableRow key={`${item.from}${item.to}`}>
              <TableCell align="right">{item.from}</TableCell>
              <TableCell align="right">{item.to}</TableCell>
              <TableCell align="right">{item.rate}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
}
export default GetUserCurrenies;
