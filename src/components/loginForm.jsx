import React from "react";
import Joi from "joi-browser";
import axios from "axios";
import Form from "./common/form";

class LoginForm extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = () => {
    let pathname
    try {
      pathname =`/${this.props.location.data.redirectTo}`
    } catch (error) {
      pathname="/getUserCurrencies"
    }
    axios
      .post("http://localhost:9898/api/user/login", {
        username: this.state.data.username,
        password: this.state.data.password,
        platform: "Web",
        buildNo: 1
      })
      .then(res => {
        if (res.data.code === 0) {
          document.cookie =`token=${res.data.content.token}`
          this.props.history.push({
            pathname,
            data: { token: res.data.content.token }
          });
        }
        else alert(res.data.message)
      });
    console.log("Submitted");
  };

  render() {
    return (
      <div className="m-4">
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Login")}
        </form>
      </div>
    );
  }
}

export default LoginForm;
